import time

# para generar numero aleatorio
import random

def proceso(nombre_proceso, tiempo_ejecución):
    # para simular un retardo aleatorio en una fábrica real
    retardo = random.randint(0,10)
    tiempo = tiempo_ejecución + retardo
    print(f'Empezando el proceso: {nombre_proceso}')
    time.sleep(tiempo)
    print(f'Terminado el proceso: {nombre_proceso}')

proceso(nombre_proceso='injection_machine', tiempo_ejecución=10)
proceso(nombre_proceso='preform', tiempo_ejecución=10)