# Importación de la librería Dash
from dash import Dash, html, dcc, callback, Output, Input
# importación plotly. Componentes gráficos
import plotly.express as px
# Importar la biblioteca que se llama pandas! Muy popular para transformación en sistemas de análisis
import pandas as pd

# Lectura de un archivo tipo CSV. separados por coma
# variable df. Dataframe. Marco de datos. Forma de tabla.
df = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/gapminder_unfiltered.csv')

# la aplicación
app = Dash(__name__)

app.layout = html.Div([
    html.H1(children='Titulo de la aplicación', style={'textAlign':'center'}),
    dcc.Dropdown(df.country.unique(), 'Canada', id='dropdown-selection'),
    dcc.Graph(id='graph-content')
])

@callback(
    Output('graph-content', 'figure'),
    Input('dropdown-selection', 'value')
)
def update_graph(value):
    dff = df[df.country==value]
    return px.line(dff, x='year', y='pop')

if __name__ == '__main__':
    app.run(debug=True)
