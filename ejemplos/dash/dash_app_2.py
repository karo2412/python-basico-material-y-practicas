# Es para importar la biblioteca Dash
from dash import Dash, html

app = Dash(__name__)

app.layout = html.Div([
    html.Div(children='Hola a todos! Buenas Noches!')
])

if __name__ == '__main__':
    app.run(debug=True)
