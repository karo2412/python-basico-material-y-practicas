"""
1- Diseñe un programa que dada una lista x = [3, -5, -1, -9, 1, 3], sustituya cualquier elemento negativo
por cero. Muestre el resultado.
"""

x = [3, -5, -1, -9, 1, 3]

resultado = []
for valor in x:
    if valor < 0:
        resultado.append(0)
    else:
        resultado.append(valor)

print(resultado)
# salida en pantalla: [3, 0, 0, 0, 1, 3]

"""
2- Diseñe un programa que dada una lista x = [“manzana”, “pera”, “uva”, “sandia”, “melon”] generar
una lista con los elementos de índice par y generar una lista con los elementos de índice impar.
Muestre el resultado.
"""
x = ["manzana", "pera", "uva", "sandia", "melon"]

#opcion 1
par = x[::2]
print('frutas de indice par', par)
impar = x[1::2]
print('frutas de indice impar', impar)

# opcion 2
par = []
impar = []
for indice, fruta in enumerate(x):
    if indice % 2 == 0: # indice par
        par.append(fruta)
    else:
        impar.append(fruta)


print('frutas de indice par', par)
print('frutas de indice impar', impar)

"""
3- Diseñe un programa que dada una lista x = [“carlos”, “andres”, “juan”, “felipe”]. Crear una lista con
el orden inverso de x. Muestre el resultado.
"""

x = ["carlos", "andres", "juan", "felipe"]

# opcion 1
x.reverse()

# opcion 2
x[::-1]

"""
4- Diseñe un programa que dada una lista x = [1,2,3,4,5]. Sume todos los valores de la lista x. Muestre
el resultado.
"""

# opcion 1
x = [1,2,3,4,5]
resultado = sum(x)
print(resultado)

# opcion 2
resultado = 0
for valor in x:
    resultado += valor
print(resultado)

"""
5- Diseñe un programa que dada una lista x = [5, 6, 10, 13, 3, 4]. Calcule el promedio de los valores en
la lista x. Muestre el resultado
"""

x = [5, 6, 10, 13, 3, 4]

resultado = sum(x) / len(x)
print(resultado)

"""
6- Diseñe un programa que dada una lista que contiene las alturas en centímetros de 4 grupos de
personas que determine cual grupo tiene la mayor altura. Muestre el resultado.
x = [
[177,145,167,190,140,150,180,130], # grupo 1
[165,176,145,189,170,189,159,190], # grupo 2
[145,136,178,200,123,145,145,134], # grupo 3
[201,110,187,175,156,165,156,135] # grupo 4
]
"""
grupo_1 = [177,145,167,190,140,150,180,130]

x = [
    grupo_1, # grupo 1                 # grupo 2 190
    [165,176,145,189,170,189,159,190], # grupo 2 190
    [145,136,178,200,123,145,145,134], # grupo 3 200
    [201,110,187,175,156,165,156,135] # grupo 4  201
]

# opcion 1
resultado = max(x, key=lambda x: max(x))
resultado = max(x, key=max)


def mi_maximo(elemento):
    return max(elemento)

otro_resultado = max(x, key=mi_maximo)


# opcion 2
grupo_mayor = -1
valor_mayor = -1
for n_grupo, valor in enumerate(x):
    mayor_en_grupo = max(valor)
    if mayor_en_grupo > valor_mayor:
        valor_mayor = mayor_en_grupo
        grupo_mayor = n_grupo

print(f'El grupo de mayor valor es {grupo_mayor}')