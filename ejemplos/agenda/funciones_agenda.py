import json
import sys

def pedir_cedula():
    cedula_amigo = input('Favor escribir la cedula del amigo')
    return cedula_amigo

def salir():
    print('Gracias por usar la agenda')
    sys.exit() # salir del programa

def indicar_opcion_no_valida(opcion_indicada):
    print(f'La opcion {opcion_indicada} no existe')


def verificar_contacto(una_funcion):
    agenda_contactos = leer_datos_desde_archivo()
    cedula_amigo = pedir_cedula()

    if cedula_amigo in agenda_contactos:
        # llamar a una función que paso por parametro.
        # dicha funcion va depende de la opción por utilizar
        una_funcion(agenda_contactos, cedula_amigo)
    else:
        print(f'El contacto de cédula {cedula_amigo} no existe')


def consultar_contacto(agenda_contactos, cedula_amigo):
    info = agenda_contactos[cedula_amigo]
    print(f'El contacto de cédula {cedula_amigo} es {info}' )

def elimina_guarda_contacto(agenda_contactos, cedula_amigo):
    # para eliminar un contacto del diccionario
    del agenda_contactos[cedula_amigo]
    guardar_datos_en_archivo(agenda_contactos)


# del bloque de mastrar de amigo.opcion 2
def mostrar_contacto():
    verificar_contacto(consultar_contacto)

# del bloque de borrado de amigo.opcion 3
def borrar_contacto():
    verificar_contacto(elimina_guarda_contacto)


# del bloque de mostrar telefonos.opcion 4
def mostrar_telefonos():

    # carlos : 32443
    # luis : 65442
    agenda_contactos = leer_datos_desde_archivo()

    for cedula_amigo in agenda_contactos:
        nombre = agenda_contactos[cedula_amigo]['nombre']
        telefono = agenda_contactos[cedula_amigo]['telefono']
        print(f'{nombre}: {telefono}')


def menu():
    opcion = input(
        """Favor escribir una opción:
        1- Agregar amigo.
        2- Mostrar amigo.
        3- Borrar amigo
        4- Mostrar telefonos de mis amigos
        5- Salir del programa

        favor digite la opción:
        """)

    print(f'La opción selecciona es {opcion}.')

    # un diccionario de funciones

    menu_de_funciones = {
        '1': agregar_contacto,
        '2': mostrar_contacto,
        '3': borrar_contacto,
        '4': mostrar_telefonos,
        '5': salir
    }

    # menu_de_funciones[opcion] # forma semejante de consultar al diccionario
    # ---------------------obtener cual funcion --------------------------|--|
    menu_de_funciones.get(opcion, lambda: indicar_opcion_no_valida(opcion))()
    #                                      |----la funcion del default----|


def agregar_contacto():
    agenda_contactos = leer_datos_desde_archivo()
    cedula_amigo = pedir_cedula()
    nombre_amigo = input('Favor escribir el nombre del amigo')
    telefono_amigo = input('Favor escribir el telefono del amigo')
    email_amigo = input('Favor escribir el email del amigo')

    # para agregar un amigo a la agenda

    # llamar a la funcion agregar_amigo
    #agregar_amigo(cedula_amigo, nombre_amigo, telefono_amigo, email_amigo)
    agregar_amigo(agenda_contactos, cedula=cedula_amigo, nombre=nombre_amigo, telefono=telefono_amigo, email=email_amigo)

    # guardar los datos en archivo
    guardar_datos_en_archivo(agenda_contactos=agenda_contactos)


def agregar_amigo(agenda_contactos, cedula, nombre, telefono, email):
    # vamos agregar un elemento al diccionario

    agenda_contactos[cedula] = {
        'nombre': nombre,
        'telefono': telefono,
        'email': email
    }

def guardar_datos_en_archivo(agenda_contactos):
    # nuestra agenda es un diccionarios, listas, etc
    # json
    with open('datos_agenda.json', 'w') as f:
        json.dump(agenda_contactos, f) # cual variable usar para guardar, cual archivo

def leer_datos_desde_archivo():
    with open('datos_agenda.json', 'r') as f:
        agenda_contactos = json.load(f)
    return agenda_contactos