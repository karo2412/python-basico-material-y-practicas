# listas

# vas al supermercado me dicen cosas por comprar

# los ciclos

# lista vacia
cosas_por_comprar = []

# para implementar un ciclo
cantidad = 10
for _ in range(cantidad):
    # como preguntar al usuario
    producto = input('Favor escriba el producto por comprar:')

    # si me piden huevos paramos el ciclo
    if producto == 'huevos':
        # break
        # para brincar hacia el siguiente vuelta
        cantidad += 1
        continue
    else:
        # para agregar un elemento a la lista
        cosas_por_comprar.append(producto)

print(cosas_por_comprar)

# para ordenar la lista
nueva_lista = sorted(cosas_por_comprar)
print(nueva_lista)

