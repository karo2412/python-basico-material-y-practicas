

# contexto

# tienes una funcion, que logica que parametros

def crear_pan(harina, azucar, huevo, polvo_hornear):
    print(f'voy a crear un pan. tiene {harina}, {azucar}, {huevo}, {polvo_hornear}')

#crear_pan('1kg de harina', '2 tazas de azucar', '2 huevos', '1 cda de polvo')

# al inicio de la decoracion
#print('coloco crema, frescas')

# al final de la decoracion
#print('lo meto de la refri')

def mi_decorador(una_funcion):
    def funcion_envolvente(*args, **kwargs):
        una_funcion(*args, **kwargs)
        print('coloco crema, frescas')
        print('lo meto de la refri')

    return funcion_envolvente


# decorando_pastel = mi_decorador(crear_pan)
# decorando_pastel()
#
# def crear_galleta(harina, azucar, huevo, polvo_hornear):
#     print(f'voy a crear una galleta. tiene {harina}, {azucar}, {huevo}, {polvo_hornear}')
#
# decorando_galleta = mi_decorador(crear_galleta)
# decorando_galleta()


@mi_decorador
def crear_arepa(harina, azucar, huevo, polvo_hornear):
    print(f'voy a crear una arepa. tiene {harina}, {azucar}, {huevo}, {polvo_hornear}')

@mi_decorador
def crear_empanada(masa, queso):
    print(f'Voy a crear empanada. tiene {masa} y {queso}')


crear_arepa('1 kg de harina', '1/2 azucar', '2 huevos', '1 cda de polvo')

crear_empanada('2 k de masa', '1/2 de mozarella')