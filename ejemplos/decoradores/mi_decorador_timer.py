from time import time

def timer(func):
    def f(*args, **kwargs):
        before = time()
        rv = func(*args, **kwargs)
        after = time()
        print('Tiempo transcurrido:', after-before)
        return rv
    return f