# silenciar al bullicioso
# ocupamos saber la hora
# para obtener el fecha y hora minutos segundos
from datetime import datetime

# el decorador es una funcion dentro de otra!
def no_hacer_bulla_en_la_noche(la_funcion_por_decorar):
    def envoltoreo():
        hora = datetime.now().hour
        # la condicion para permitir la bulla
        if 11 <= hora < 17:
            la_funcion_por_decorar()

    return envoltoreo

@no_hacer_bulla_en_la_noche
def cantar():
    print(' bla bla bla')

cantar()