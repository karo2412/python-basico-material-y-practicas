from time import time
from time import sleep
import random

lista_procesos = ['PET_resin','Injection_Machine','Preform',
                  'Perform_heating','Introduce_preform_into_mould_and_closing_unit',
                  'Inserting_the_switch','Stretchingand_pre_blowing',
                  'Final_bottle_blowing','Mould_Opening',
                  'Discharge_bootle_from_the_mould']

def timer(func):
    def f(*args, **kwargs):
        before = time()
        rv = func(*args, **kwargs)
        after = time()
        print(f'Tiempo transcurrido:', str(int(after-before))+' segundos')
        return rv
    return f

@timer
def proceso(nombre_proceso, tiempo_ejecucion):
    # para simular un retardo en segundos aleatorio en una fábrica real
    retardo = random.randint(0,10)
    tiempo = tiempo_ejecucion + retardo
    print(f'Empezando el proceso: {nombre_proceso}')
    sleep(tiempo)
    print(f'Terminado el proceso: {nombre_proceso}')

# Para ejecutar los procesos
for mi_proceso in lista_procesos:
    proceso(nombre_proceso=mi_proceso, tiempo_ejecucion=10)