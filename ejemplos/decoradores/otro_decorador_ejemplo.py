
from time import sleep
from mi_decorador_timer import timer


@timer
def add(x, y=10):
    return x + y

@timer
def sub(x, y=10):
    return x - y

@timer
def crear_arepa(harina, azucar, huevo, polvo_hornear):
    print(f'voy a crear una arepa. tiene {harina}, {azucar}, {huevo}, {polvo_hornear}')


@timer
def perder_tiempo(tiempo_a_dormir=10):
    print('estoy tratando de dormir')
    sleep(tiempo_a_dormir)
    print('me estoy despertando')

@timer
def realizar_ejercicio():
    print('estoy caminando')
    sleep(7)
    print('termine de caminar')

perder_tiempo(1)
realizar_ejercicio()

# print('add(10)', add(10))
# print('add(10,20)', add(10,20))
# print('sub(10,20)', sub(10,20))
#
# crear_arepa(harina=1, azucar=2, huevo=10, polvo_hornear=1)
