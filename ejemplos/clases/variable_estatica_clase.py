# como hacer que la clase tenga una variable
# que este en comun en todos los objetos
from datetime import datetime

def crear_id():
    import random
    offset = random.randint(0, 1000)
    return int(datetime.now().timestamp() * 1e6) + offset

class Carrito(dict):
    contador_de_carrito = 0
    def __init__(self):

        self.identificador = crear_id()
        self.lista_articulos = []
        self.incrementar_carrito()


    @classmethod
    def incrementar_carrito(cls):
        cls.contador_de_carrito += 1

    def pagar(self):
        print('pagando')

    def agregar_item(self, item):
        self.lista_articulos.append(item)


# en barrador vou a a crear un diccionario de carritos
inventario_carrito = {}

def agregar_carrito():
    inventario_carrito.update({
        crear_id(): Carrito()
    })

agregar_carrito




pass