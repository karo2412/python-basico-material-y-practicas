
# circulos

class Circulo:
    def __init__(self):
        self._radio = 0

    # mi getter
    @property
    def radio(self):
        return self._radio

    @property
    def area(self):
        from math import pi
        return self.radio ** 2 * pi


    # mi setter
    @radio.setter
    def radio(self, valor):
        # puede ser negativo ? pues no
        if valor < 0:
            print('El radio no puede ser negativo')
        else:
            self._radio = valor # aqui es la linea clave! aqui se actualiza!!


mi_circulo = Circulo()

# mi_circulo.radio(valor =10) # caso malo

mi_circulo.radio = 20
mi_circulo.radio = -10 # usando el setter

# usar el getter
print(f'El valor del radio de mi circulo es {mi_circulo.radio}')

print(f'El area del circulo es {mi_circulo.area}')