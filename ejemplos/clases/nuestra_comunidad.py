
# vamos a crear una class de una Persona
# nombre
# id

class Persona:

    def __str__(self):
        return f'{self.nombre}, id:{self.id}'

    def __repr__(self):
        return f'{self.nombre}, id:{self.id}'

    # definir un constructor
    def __init__(self, nombre, id):
        self.nombre = nombre
        self.id = id
        self.patrimonio = 0

    # ganar dinero trabajando en un lugar
    def trabajar(self):
        self.patrimonio += 2000

# juan = Persona(nombre='juan', id='31234456')
# maria = Persona(nombre='maria', id='456788')


lista_amigos = ['juan', 'pedro', 'maria', 'susana', 'luis']
# vamos a suponer que el id viene de la posición del amigo en la lista

listas_personas = []
for posicion, nombre in enumerate(lista_amigos):
    amigo = Persona(nombre=nombre, id=posicion)
    listas_personas.append(amigo)


# quiero poner a trabajar a maria. no se que en que posición esta maria.
for persona in listas_personas:
    if persona.nombre == 'maria':
        persona.trabajar()
pass