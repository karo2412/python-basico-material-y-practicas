
mi_diccionario = dict(key1='123', key2=456)
pass


# key: value
# 'leche': 1000

class Producto(dict):
    pass


# herencia con diccionario
class Carrito(dict):
    identicador = 0
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.identicador += 1
        self.lista_de_articulos = []

    def comprar(self, producto):
        
        self.lista_de_articulos.append(producto)

    def pagar(self):
        saldo = 0
        for articulo in self.lista_de_articulos:
             saldo += articulo['cantidad'] * articulo['precio']
        return saldo

        # return sum([precio for precio in self.values()])
        #return sum(self.values())


mi_carrito = Carrito()
mi_carrito_1 = Carrito()
mi_carrito.comprar(Producto(nombre='leche', cantidad=2, precio=1000))
mi_carrito.comprar(Producto(nombre='arroz', cantidad=4, precio=2000))
mi_carrito.comprar(Producto(nombre='queso', cantidad=1, precio=3000))
# mi_carrito['leche'] = 1000
# mi_carrito['arroz'] = 1600
# mi_carrito['azucar'] = 800
saldo = mi_carrito.pagar()
pass