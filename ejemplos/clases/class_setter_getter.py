
class Persona:
    def __init__(self):
        # ojo con el nombre del atributo
        # el atributo tiene que tener un nombre diferente
        self._edad = 0

    # configurando el metodo getter
    @property
    def edad(self):
        print(f'Estoy ejecutando el getter')
        return self._edad


    # configurando el metodo setter
    @edad.setter
    def edad(self, mi_edad):
        if mi_edad < 18:
            # raise ValueError("No puedo cambiar la edad")
            print('no puedo cambiar')
        print('soy mayor de edad puedo cambiar mi edad')
        self._edad = mi_edad

juan = Persona()

try:
    juan.edad = 'hola'
    juan.edad = 10
    juan.edad = 21
    juan.edad = 8

except ValueError as err:
    print(f'Ha pasado un error con un valor. mas detalles en {err}')

except Exception as err: # tratamiento de error generico
    print('Ups algo pasó')

