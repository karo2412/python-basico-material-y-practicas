import random
# que caracteristica tiene la persona
# altura
# cedula
# nombre
# apellido

class Persona:
    def __init__(self, nombre, apellido, cedula):
        self.nombre = nombre
        self.apellido = apellido
        self.cedula = cedula
        self.altura = random.randint(30, 50)
        self.peso = random.randint(1, 3)

    def crecer(self):
        self.altura += 1
        #return self.altura

    def hablar(self, mensaje):
        # dice hola soy
        print(f'Hola soy {self.nombre} y digo {mensaje}')

    def __str__(self):
        # mi intención es ser capaz de convertir el objeto Persona -> str
        # debe un texto
        return f'{self.nombre} {self.apellido}'


if __name__ == '__main__':
    # instantiation
    juanito = Persona('juan', 'perez', '123456789')
    maria = Persona('maria', 'castro', '987654321')
    carlitos = Persona('carlos', 'alfaro', '6774373')
    andrecito = Persona('andres', 'salas', '321321')


    mis_amigos = [juanito, maria, carlitos]
    mis_amigos.append(Persona('andres', 'salas', '1826123'))

    for mi_amigo in mis_amigos:
        print(f'Yo tengo un amigo que se llama {mi_amigo}')


    agenda_de_amigos = {'carlos' : carlitos,
                        'andres': andrecito,
                        'juan': juanito,
                        'maria': maria
                        }

    pass