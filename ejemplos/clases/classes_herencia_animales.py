

class Perro:
    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad

    def __str__(self):
        return f'{self.nombre} tiene {self.edad} años'

    def ladrar(self, ladrido):
        print(f'El perro {self.nombre} esta diciendo {ladrido}')


class Ladrador(Perro):
    def __init__(self, pelo_largo=True, *parametros_posionales, **parametros_por_keyword):
        super().__init__(*parametros_posionales, **parametros_por_keyword)
        self.pelo_largo = pelo_largo

    def babear(self):
        print(f'{self.nombre} está babiendo')

    def __str__(self):
        return f'Soy un un Perro labrador'

bobby = Perro(nombre='bobby', edad=5)
lassie = Ladrador(nombre='lassie', edad=2, pelo_largo=False)

lassie.babear()
bobby.ladrar('wuau')

lassie.ladrar('wooaw')
print(f'{lassie.nombre} tiene {lassie.edad}')