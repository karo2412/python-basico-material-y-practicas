from datetime import datetime

class Persona:
    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad

    def __str__(self):
        return f'{self.nombre} tiene {self.edad} años'

    @classmethod
    def usando_el_anno_de_nacimiento(cls, nombre, anno):
        edad = datetime.now().year - anno
        return cls(nombre, edad)

    @classmethod
    def creando_el_hijo_del_rey(cls, edad):
        return cls(nombre='principe', edad=edad)


juan = Persona('juanito', 10) # usando la construccion original
print(juan)

maria = Persona.usando_el_anno_de_nacimiento('maria', 1988)
print(maria)

el_principe = Persona.creando_el_hijo_del_rey(20)
print(el_principe)

