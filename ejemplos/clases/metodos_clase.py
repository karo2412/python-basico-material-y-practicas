class pizza:
    def __init__(self, ingredientes):
        self.ingredientes = ingredientes

    def __str__(self):
        return f'pizza({self.ingredientes})'

    def __repr__(self):
        return f'Pizza({self.ingredientes})'

    @classmethod
    def margarita(cls):
        return cls(['mozzarella', 'tomate'])

    @classmethod
    def suprema(cls):
        return cls(['mozzarella', 'tomate', 'jamon'])

    @classmethod
    def hawallana(cls):
        return cls(['piña', 'tomate', 'chile'])


    @classmethod
    def promocion(cls, apellido):
        # vargas. suprema
        # alfaro. margarita
        # sanchez. hawallana
        opciones = {
            "vargas": cls.suprema(),
            "alfaro": cls.margarita(),
            "sanchez": cls.hawallana()
        }
        return opciones.get(apellido)

pizza_para_alfaro = pizza.promocion(apellido='alfaro')
pizza_para_sanchez = pizza.promocion(apellido='sanchez')
pass
