# diccionario

#creacion de diccionarios!!!
# refactor

#las llaves ya no queiro que sea humildes strings
# quiero que sean tuplas
# (nombre, apellido)
agenda = {
    ('carlitos', 'mora'): 7461252,
    ('luis', 'hernandez'): 85215125,
    ('andres', 'salas'): 43215231,
    ('maria', 'jimenez'): 64326423,
    ('susana', 'castro'): 987721,
    ('maria', 'vargas'): 10101010010101
          }

# leer informacion
# resultado = agenda['juan']
# print(resultado)

# consulta
resultado = agenda.get(('maria', 'vargas'), None)

# verifica si la variable resultado hace referencia al None
# if resultado == None:   <- por favor NO!
if resultado is None:
    print('el dato no está disponible')
else:
    print(resultado)

# expresiones regulares!!

# como se obtienen todas las llaves?
print(agenda.keys())

# como obtengo los numeros de telefonos de todos mis amigos
print(agenda.values())

# como obtengo todos los datos?
print(agenda.items())

mis_datos = [(('carlitos', 'mora'), 7461252),
             (('luis', 'hernandez'), 85215125),
             (('andres', 'salas'), 43215231),
             (('maria', 'jimenez'), 64326423),
             (('susana', 'castro'), 987721),
             (('maria', 'vargas'), 10101010010101)
             ]

pass


# como actualizar los datos?
agenda['maria'] = 8327131

agenda.update( {'carlitos': 532535} ) # actualizar llaves presentes
agenda.update( {'juan': 321238} ) # agregar
pass

# compras al super!
# juancto va al super
# pan 400, queso 500, salsa 1000, jamon 700
# paso 1. crear un diccionario con las compras de juan. las llaves seran los nombres de los productos.
# valores seran el precio de cada producto

juan = {
    'pan': 400,
    'queso': 500,
    'salsa': 1000,
    'jamon': 700
}

# paso 2: el precio del queso tiene un 15% de descuento. actualizar el dato del precio del queso
descuento = 0.15
#juan.update({'queso': juan['queso'] * (1-descuento) })

#opcion 2
#juan['queso'] = juan['queso'] * (1-descuento)

#opcion 3

#juan['queso'] *= 1 - descuento


# paso 3. se acabo del jamon. juan no puede comprar jamon
# opcion 1
#del juan['jamon']

#opcion 2

info_jamon = juan.pop('jamon')

juan['jamon'] = info_jamon

pass