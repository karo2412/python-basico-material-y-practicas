# 1.  Escriba un script de Python para ordenar (ascendente y descendente) un diccionario por valor.
# Ejemplo de diccionario : {0: 10, 1: 20, 2:30}


mis_datos = {
    0: 7,
    1: 1,
    2: 3
}

# quiero ver los datos valor por valor para ordernar por valor
# saberes:
# necesito saber como iterar diccionarios
# necesito saber como agregar datos a uno diccionario

# lista de tuplas
# [llave1: valor, llave2:valor]
lista_tuplas = list(mis_datos.items())

def criterio_ordenar_por_valor(dato):
    return dato[1]

# necesito orderar la variable lista_tuplas por el 'valor'
resultado = sorted(lista_tuplas, reverse=True, key=criterio_ordenar_por_valor)

resultado = sorted(lista_tuplas, reverse=True, key=lambda dato: dato[1])


# convertir de lista de tuplas a diccionario basta con ejecutar la función dict()
print(dict(resultado))
pass