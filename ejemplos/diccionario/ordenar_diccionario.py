datos = {
    'c': 2,
    'a': 0,
    's': 1,
    'e': 3
}


def my_func(item):
    return datos[item]

resultado = sorted(datos, key=my_func)
print(resultado)