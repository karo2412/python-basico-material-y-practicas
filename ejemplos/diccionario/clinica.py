# tipo de estrutura
# diccionario de diccionarios

clinica = {
    '123' : {'nombre': 'carlos',
             'telefono': '9876231',
             'enfermedades': ['fiebre', 'dolor de cabeza']
             },
    '678': {'nombre':'maria',
            'telefono': '6326',
            'enfermedades': ['gripe', 'covid', 'fiebre']

    }
}


def enfermedades_clinica():
    enfermedades_de_la_clinica = []

    # .keys() solo las llaves
    # .items() todo el diccionario. en forma de lista de tuplas
    # .values() los datos del diccionario (values)

    for paciente in clinica.values():
        enfermedades = paciente['enfermedades']
        enfermedades_de_la_clinica.extend(enfermedades)
    return list(set(enfermedades_de_la_clinica))

enfermedades_clinica()

def agregar_paciente(id, nombre, telefono):
    clinica.update(
        {id:
             {'nombre': nombre,
              'telefono': telefono}
         }
    )

def agregar_enfermedad(id, enfermedad):
    clinica[id]['enfermedades'].append(enfermedad)

# agregar_paciente(id='345', nombre='juan', telefono='456456')
# agregar_enfermedad(id='123', enfermedad='azucar')

opciones = {'1': agregar_paciente,
            '2': borrar_paciente,
            '3': agregar_enfermedad,
            '4': agregar_medicina}

pass