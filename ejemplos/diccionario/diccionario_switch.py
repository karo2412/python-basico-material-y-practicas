
# mas complejo que if else

# multiples condiciones!!! importantes

# switch


# ejemplo:
# carlos manzana
# andres pera
# luis mango

amigos = {'carlos': 'manzana',
          'andres': 'pera',
          'luis': 'mango'
          }

amigos['carlos']

# funciones!!! hacemos actividades!!

def ciclismo(accesorio, lugar, *args, **kwargs):
    if len(kwargs) > 0:
        addicional = kwargs.items()
    else:
        addicional = ''
    print(f'Estoy pedaleando con {accesorio} en {lugar} {addicional}')

def natacion(accesorio, lugar, *args, **kwargs):
    print(f'Estoy braceando con {accesorio} en {lugar}')

def tenis(accesorio, lugar, *args, **kwargs):
    # join
    accesorios_opciones = ', '.join(args)

    print(f'Estoy golpeando la bola con {accesorio} en {lugar} {accesorios_opciones}')

amigos = {'1': crear_paciente,
          '2': borra_paciente,
          'luis': tenis
          }

amigos['carlos']

#mi_accesorio = input('Que usa carlos para hacer ciclismo?')

accesorio = {'ciclismo': 'bicicleta', 'natacion': 'anteojos', 'tenis': 'raqueta'}

# Invocacion. llamar a la funcion!!
amigos['carlos'](accesorio='bicicleta', lugar='montaña', casco='M', guantes='L') # colocando valor al parametros mediante keyword (explícita)
amigos['andres']('anteojos', 'polideportivo', 'bloqueador solar') # colocando valor al parametro por posicion (implicita)
amigos['luis']('raqueta', 'tenis club', 'gorra', 'bola', 'zapatillas', 'net')