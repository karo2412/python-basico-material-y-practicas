
# como crear diccionarios

# agenda de contactos, amigos, etc
#          llave     valor        llave  valor
agenda = {
    'carlos': '(+1 111)88887777',
    'luis': '4444333'
}

# como agregar. andres 89787544
#              un dict
agenda.update( {'andres': '89787544'} )

# otra forma de agregar. maria 4578457
agenda['maria'] = '4578457'

#print(agenda)

# deseo saber el # de telefono de carlos
#print(agenda['carlos'])

# andres . me dio el direccion, email, cedula
#               key     value
agenda.update({'andres': {'direccion': 'avenida siempre viva',
                          'email': 'andres@aol.com',
                          'cedula': '2-1111-2222',
                          'telefono': agenda['andres']
                          }})

agenda.update({'susana': ['susana@gmail.com', 'heredia', 'mani']})

#print(agenda)

# ciclo en los diccionarios

# mostrar en pantalla el nombre del amigo junto al telefono
for nombre in agenda:
    # ocupamos que revisar si es texto o diccionario

    telefono = 'Dato no disponible'
    if isinstance(agenda.get(nombre), str):
        telefono = agenda.get(nombre)

    if isinstance(agenda.get(nombre), dict):
        telefono = agenda.get(nombre).get('telefono')
    mensaje = f'Quiero llamar a {nombre} el telefono es {telefono}'
    print(mensaje)