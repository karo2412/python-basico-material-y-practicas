# la vamos a pedir los datos los datos al usuario
# vamos usar la función input
# resultado = input('algun mensaje')

# El input retorna únicamente texto!!!!

# la función int : convierte a número entero
# int()

a = int(input('Favor escriba el valor de a:'))
b = int(input('favor escriba el valor de b:'))
c = int(input('favor escriba el valor de c:'))

if a > b:
    if a > c:
        maximo = a
    else:
        maximo = c
else:
    if b > c:
        maximo = b
    else:
        maximo = c

print(f'El valor máximo es: {maximo}') # darle formato al texto de que manera que se muestra un mensaje personalizado usando el f-string