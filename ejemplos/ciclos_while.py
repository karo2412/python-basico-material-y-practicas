# listas

# vas al supermercado me dicen cosas por comprar

# los ciclos

# lista vacia
cosas_por_comprar = []

# para implementar un ciclo
comprar = True
while comprar:
    # como preguntar al usuario
    producto = input('Favor escriba el producto por comprar:')

    # agregar una condicion para parar
    # utilizar el datos -1
    if producto == "-1":
        break
        #comprar = False

    # si me piden huevos paramos el ciclo
    if producto == 'huevos':
        # break
        # para brincar hacia el siguiente vuelta
        continue
    elif producto not in cosas_por_comprar:  # verificar si no existe mediante el operador de pertenencia
        # para agregar un elemento a la lista
        cosas_por_comprar.append(producto)

print(cosas_por_comprar)

# para ordenar la lista
nueva_lista = sorted(cosas_por_comprar)
print(nueva_lista)

