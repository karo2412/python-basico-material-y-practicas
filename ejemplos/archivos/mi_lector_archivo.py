# vamos leer archivos
# vamos a suponer que el archivo


with open('mi_informacion.txt', 'r') as mi_archivo:
    mi_texto = mi_archivo.readlines()

with open('mi_historia.txt', 'r') as mi_archivo:
    mi_historia = mi_archivo.readlines()
# list comprehension
# muy similar a hacer ciclos
# sabiendo que la variable mi_texto es una lista

mi_texto_procesado = [elemento.strip() for elemento in mi_texto]

# new line  \n
print(mi_texto)
