# una biblioteca json

import json
import os

amigos = {'carlos': 'carlos@aol.com',
          'luis': 'luis@gmail.com',
          'andres': 'andres@hotmail.com',
          'juan': 'juan@yahoo.com'}

# # vamos a escribir
# with open('c:/mi_carpeta/mis_amigos.json', mode='w') as f:
#     json.dump(amigos, f)

with open('c:/mi_carpeta/datos_trabajo.json', mode='r') as f:
    datos = json.load(f)


class Amigos(dict):
    pass

mis_amigos = Amigos(**datos)
pass