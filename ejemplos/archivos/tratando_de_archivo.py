
# necesito leer un archivo que no se que si existe o no
# que pasa si no existe: voy a proponer unos datos por default

try:
    with open('mi_archivo.txt', 'r') as f:
        datos = f.read()

    a = 1 / 0

except FileNotFoundError as error:
    print(error)
    datos = 'Datos predeterminados'

except ZeroDivisionError as error_division_zero:
    # necesito mas cosas!
    print(error_division_zero)

# tipo de excepcion generico
except Exception as error_generico:
    print(error_generico)

print(datos)

# glob