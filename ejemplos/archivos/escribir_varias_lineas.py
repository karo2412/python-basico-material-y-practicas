
# vamos a escribir varios reglones o lineas de texto!

mi_informacion = [
    'buenos días\n',
    'Mi nombre es Juanito\n',
    # 'hasta luego'
]

with open('saludos.txt', 'a') as mi_archivo:
    mi_archivo.writelines(mi_informacion)

# el modo 'w' permite hacer sobreescribitura del archivo
# el modo append 'a permite agregar al final del archivo