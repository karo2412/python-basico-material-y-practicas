
import pickle

mis_datos = {
    'carlos': {'email': 'carlos@aol.com', 'cel': 123},
    'andres': {'frutas': ['pera', 'piña'], 'diabetes': None},
    'luis': {'gluten': True}
}

# escritura binaria
with open('mis_amigos.p', 'wb') as f:
    pickle.dump(mis_datos, f, pickle.HIGHEST_PROTOCOL)