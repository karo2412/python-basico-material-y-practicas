

# se supone que vamos a leer archivos!
# te dicen que el archivo datos.txt existe

# ocupamos decir al usuario que el archivo no existe

# el tratamiento de las excepciones!

try:
    with open('datos.txt', 'r') as mi_archivo:
        mi_variable = mi_archivo.read()

except:
    print('El archivo no existe!')