# tengo una tuplas
valores = (True, True, True)

# quiero saber si al menos alguno de los elementos de la tupla
# valores es un True

# sera que algunos son True
resultado = any(valores)
print(resultado)

# ""si al menos hay un False""

# Verifica: si todos los elementos son True
resultado = not all(valores)
print(resultado)