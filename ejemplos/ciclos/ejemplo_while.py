
# suponga que quiero mostrar mostrar en pantalla la palabra "hola!" indefinidamente
# quiero mostrar el mensaje cada segundo!!!

# desde el package time quiero importar la funcion sleep
from time import sleep
from datetime import datetime

valor = 0
while valor < 5: # True -> False
    print(f"hola! tengo el valor de {valor}") # f-string
    valor += 1 # autoincremento

    print(f'Fecha y hora es {datetime.now()}') #  f-string
    sleep(1.5) # duerme un segundo!

print('he terminado...')