
#           0,    1,       2,          3
frutas = ['cas', 'coco', 'aguacate', 'pera']
# mostrar en pantalla información relevante a cada una de las frutas. una por una
# quiero calcular el tamaño de cada nombre de la frutas
# quiero saber el numero de fruta según la posición en la lista

# usando un ciclo

# iteración
for posicion, fruta in enumerate(frutas):
    tamano = len(fruta)
    print(f'La fruta {fruta} tiene {tamano} letras y esta en la posicion {posicion}') # f-string