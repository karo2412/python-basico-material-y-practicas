
# juan fue al supermercado

juan = [
    ('leche', 1000),
    ('manzana', 800),
    ('confite', 50),
    ('cafe', 5000),
    ('helado', 100),
    ('confite', 10),
    ('chicle', 40)
]

# cual es el total de sólo confites!

resultado = list(filter( lambda x: x[0] == 'confite', juan)) # el filtrado
el_total_confites = sum([precio for _, precio in resultado]) # sumarizacion!

print(el_total_confites)


