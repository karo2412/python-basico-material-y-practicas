

# quiero hacer una calculadora

# que hacer las siguientes las operaciones
# suma
# resta
# multiplicacion
# division

# podriamos crear diccionario
# llave: algo _ value

calculadora = {
    'suma': lambda x,y : x + y,
    'resta': lambda x,y : x - y,
    'multiplicacion': lambda x,y: x * y,
    'division': lambda x, y  :  x / y
}

# queremos sumar 2 números
a = 100
b = 2

resultado = calculadora['suma'](a, b)
print(resultado)