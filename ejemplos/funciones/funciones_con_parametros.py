
def mi_funcion_con_parametros(datos1, datos2):
    # f-string
    return f'Estoy comiendo {datos1} con {datos2}'

resultado = mi_funcion_con_parametros(["papas", "zanahoria"], 100)
print(resultado)

