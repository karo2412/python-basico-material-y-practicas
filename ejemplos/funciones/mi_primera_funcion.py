
# estamops creando la funcion sumar
def sumar(a, b):
    return a + b

if __name__ == '__main__':
    # vamos a usar la funcion sumar
    c = sumar(2, 5)
    print(c)

    print(sumar(10, 10))

    # sumar los los numeros en ciclos

    for mi_numero in range(10):
        print(sumar(mi_numero, 100))