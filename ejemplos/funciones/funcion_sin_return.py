

def mi_funcion_sin_retorno():
    print('Hola esta es una funcion sin retorno!')

resultado = mi_funcion_sin_retorno()
print(resultado)

# como verifica si una vriable es nula o no

if resultado is None:
    print('la funcion no retorna dato')
else:
    print('la funcion retorna dato')