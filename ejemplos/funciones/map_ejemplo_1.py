
# los siguientes son los precios de frutas

juan = [100, 400, 500, 2000]

# se desea calcular el precio final que juan debe pagar
# vamos aplicar el impuesto a cada producto
impuesto = 0.13

#map( lambda x : x + x * impuesto , juan)
# contabilidad
# tomar en cuenta el redondeo

resultado = map( lambda x : x * (1 + impuesto) , juan)
total = round(sum(resultado), 2)
pass