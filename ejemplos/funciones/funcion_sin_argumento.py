

# quiero mostrar la fecha del sistema

from datetime import datetime
from time import sleep

def mostrar_fecha_hora():
    fecha = datetime.now()
    print(fecha)

# mostrar la hora
mostrar_fecha_hora()

# dormir un rato
sleep(10)

# mostrar la hora
mostrar_fecha_hora()
