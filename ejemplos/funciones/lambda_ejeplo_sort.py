# estructura: lista de tuplas

a = [
    (1, 4),  # mean(1, 4)
    (5, 2),  # mean(5, 2)
    (9, 0),
    (3, 1),
    (0, -1)
]

# # ordenar por el segundo elemento de cada tupla
# print(a)
# fijarte en cada elemento, uno a uno -> se le pasa al lambda
# a.sort(key=lambda x: x[-1])
# print(a)

# ordenar por el promedio de cada tupla
print(a)
# va a tomar cada tupla
# (1, 4) -> x   sum(x)-> 5
a.sort(key=lambda x: sum(x)/len(x))
print(a)