
# quiero una funcion
# area y perimetro de circulo según sea el radio

from math import pi, pow

def area_perimetro_circulo(radio):
    area = pow(radio, 2) * pi

    perimetro = 2 * radio * pi

    return area, perimetro


radio1 = 10
area, perimetro = area_perimetro_circulo(radio1)
pass

