# quiero hacer una funcion que simplemente suma
# 2 datos

# fórmula del círculo
# area = r ^ 2 * pi
#pi = 3.1415

from math import pi, pow

def area_del_circulo(radio):
    return radio ** 2 * pi  #  pow(radio,2)

# print(area_del_circulo(2))
#
# print(area_del_circulo(3))
#
# print(area_del_circulo(5))

# que tengo que hacer para calcular las áreas de los
# círculos de radio 1 hasta 10. 1 , 2, 34, 5, 6, 7, 8, 9, 10

# iterar: ciclos for: for-loop
# generar automatica la secuencia del 1 a 10
# range(inicio, final, tamano_del_salto:1)

for radio in range(1, 11):
    print(f'El área del círculo de radio {radio} es {area_del_circulo(radio)}')
