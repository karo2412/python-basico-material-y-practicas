# ejemplos con conjuntos

carlitos = {'manzana', 'uva', 'pera'}

andres = {'mora', 'uva', 'coco'}

# cual fruta tienen en comun
# interseccion!

resultado = carlitos.intersection(andres)
print(resultado)

carlitos & andres  # intersección

# cuales son las frutas que tenemos si carlitos
# y andres las comparten
resultado = carlitos | andres # union
print(resultado)

# cuales frutas tiene carlitos o andres pero no ambos
resultado = carlitos ^ andres
print(resultado)

# cuales son las frutas de carlitos
# que no estan en comun con andres
resultado = carlitos - andres
print(resultado)


# cuales son las frutas de andres que no
# tiene carlos
resultado = andres - carlitos
print(resultado)

# pertenencia
# si andres le gusta el coco
resultado = 'coco' in andres
print(resultado)


# duplicados
# luis fue al supermercado y compro lo siguiente
#luis -> pera, limon, cas, tomate, pera, limon, naranja
#limon, tomate, cas
# luis, cuales frutas compraste?

# crear un conjunto
lista_fruta = ['pera', 'limon', 'cas', 'tomate',
               'pera', 'limon', 'naranja',
               'limon', 'tomate', 'cas']

# set : es para conjuntos desde otra estructura de datos
las_frutas_que_compro_luis = set(lista_fruta)
print(las_frutas_que_compro_luis)

{'cereza', 'durazno', 'kiwi', 'caimito'}

pass


# ejercicio

a = set('abracadabra')
b = set('alacazam')

#1 cuales letras tienen un comun?
# intersection
print('pregunta1', a, b, a & b)
a & b
#2. cuales letras tiene 'abracadabra' que no tiene 'alacazam'

# diferencia
a-b
print('pregunta2', a, b, a - b)
#3 cuales son las letras que tiene 'abracadabra' o 'alacazam'
# union
a | b
print('pregunta3',a, b, a | b)

{1,2,3,4,5,6,7}
{321,643,987,12,654,723}
pass