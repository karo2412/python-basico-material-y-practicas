
# Para crear una lista vacia
my_empty_list = []

# los vacio lo podemos llenar
# mediante el uso de la función append

my_empty_list.append(100)
print(my_empty_list)

# print('Mi primera lista... {0}, es del tipo... {0}'.format(my_empty_list, type(my_empty_list)))
# # antes Mi primera lista... [], es del tipo... <class 'list'>

# None ---> representa. datos faltos faltan, datos no definidos

# indices
#              0       1     2    3     4
#             -5      -4   -3    -2    -1
mis_cosas = ['reloj', 100, 3.14, True, None]
print(mis_cosas)

#my_empty_list.append(mis_cosas)


# quiero sobreescribir última posición
mis_cosas[-1] = 'adios'

print(mis_cosas)

# quiero agregar los elementos de la lista 'mis_cosas' a my_empty_list
# usando extend
my_empty_list.extend(mis_cosas)
print(my_empty_list)

my_empty_list = mis_cosas
print(my_empty_list)