
#           0        1        2          3      4
frutas = ['mango', 'pera', 'manzana', 'piña', 'kiwi']

#      inicio    final
#      una copia de la lista
print(frutas[         :     ][2])

# necesito
# primera y la segunda

resultado = frutas[0: 2]
print(resultado)

# quiero desde la pera hasta el final de la lista de frutas
resultado = frutas[1:]
print(resultado)