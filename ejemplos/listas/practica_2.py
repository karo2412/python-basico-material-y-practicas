# Se tiene la siguiente información sobre las compras de algunos amigos
# en un supermercado

compras_amigos = [
    ['leonardo', 100, 400, 50],
    ['juan', 200, 100, 20],
    ['fabián', 50, 200, 150],
    ['adolfo', 80, 100, 200],
    ['emerson', 40, 100, 500]
]

# max y min

# Quién es el amigo que gastó más?. Cuál es el monto?
# Quién es el amigo que gastó menos? Cuál es el monto?

# crear una funcion personalizada
def mi_funcion(valores_de_compra):
    precios = valores_de_compra[1:] # solo precios
    return sum(precios) # importante!!!

resultado = max(compras_amigos, key=mi_funcion)
print(resultado)

# segunda forma
resultado = max(compras_amigos, key=lambda x: sum(x[1:]))
print(resultado)

#
# def my_func(x):
#     return sum(x[1:])
#
# max(compras_amigos, key=my_func)
#
# max(compras_amigos, key=lambda x: sum(x[1:]))
# pass