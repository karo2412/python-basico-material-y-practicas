# Diseñe un programa que dada una lista que contiene
# las alturas en centímetros de 4 grupos de personas que
# determine cual grupo tiene la mayor altura.
# Muestre el resultado.

x = [
    [177,145,167,190,140,150,180,130], # grupo 1. sublista
    [165,176,145,210,170,189,159,190], # grupo 2  sublista
    [145,136,178,200,123,145,145,134], # grupo 3  sublista
    [201,110,187,175,156,165,156,135]  # grupo 4  sublista
]

# tenemos una lista de listas

# propuesta puede ser usando la función max

def mi_funcion(grupo):
    return max(grupo)

resultado = max(x, key=mi_funcion)
#resultado = max(x, key=max)
el_grupo_con_la_altura_mayor = x.index(resultado)
print(f'El grupo {el_grupo_con_la_altura_mayor + 1} tiene la mayor altura')
pass