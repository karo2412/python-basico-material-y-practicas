#           -5       -4       -3      -2        -1
#           0         1        2       3         4
frutas = ['mango', 'mamon', 'cas', 'naranja', 'nance']

# utilizando slicing
# slicing . deme toda la lista
frutas[:]

# desde el segundo elemento
frutas[1:]

#       inicio  final    salto
frutas[        :       :       ]

# las frutas cuyo indice (posicion) par
# no significa nada.