frutas = ['apple', 'grape', 'blackberry', 'orange', 'cas', 'kiwi', 'albaricoque']
print(frutas)



# obtener el maximo por el tamaño de la palabra
# la palabras mas larga
# la función max

print(max(frutas))

# el len: se obtiene la longitud de la lista
# al len: se lo vamos aplicar a una coleccion de caracteres:
# un string -> la longitud del string, la longitud de la palabras
resultado = max(frutas, key=len)
print(resultado)

# la palabra mas corta. el nombre mas corto
mas_corta = min(frutas, key=len)
print(mas_corta)

#frutas.sort
#sorted(frutas)

frutas.sort(key=len)
print(frutas)


# ejemplo

# las compras de los amigos
amigos = [
    [1000, 1500], # amigo 1
    [400, 3000],  # amigo 2
    [500, 1000]   # amigo 3
]

# cual elemento de la lista amigos tiene la mayor en total

# la función sum: en las listas -> suma los elementos
resultado = max(amigos, key=sum)
print(resultado)


resultado = min(amigos, key=sum)
print(resultado)