# la lista y sus funciones

frutas = ['mango', 'pera', 'fresas', 'banano', 'uva', 'naranja', 'zapote']
print(frutas)

# para agregar
frutas.append('piña')
print(frutas)

# para agregar varias frutas
frutas.extend(['manzana', 'sandia'])
print(frutas)

# para ordenar la lista de frutas
frutas.sort()
print(frutas)

# quiero obtener el primer fruta
primera_fruta = frutas[0]
print(primera_fruta)

# ultima fruta
ultima_fruta = frutas[-1]
print(ultima_fruta)

# penultima fruta fruta

penultima_fruta = frutas[-2]
print(penultima_fruta)

# para ordenar basado en la ultima letra de las frutas
# funciones anonimas. lambda

# primer ejemplo con la funcion lambda.

# aguacate . la ultima letra es e.
'aguacate'[-1]

frutas.sort(key= lambda x: x[-1])
print(frutas)

# ordenar por la longitud de cada nombre de fruta
frutas.sort(key=lambda x: len(x))
print(frutas)

# ordenar por la longitud de cada nombre de fruta (en reversa)
frutas.sort(key=lambda x: len(x), reverse=True)
print(frutas)

# obtener el mayor
print(max(frutas))

# obtener el nombre de la fruta más larga
print(max(frutas, key=lambda x: len(x)))