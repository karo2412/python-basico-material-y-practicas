# Diseñe un programa que dada una lista que contiene
# las alturas en centímetros de 4 grupos de personas que
# determine cual grupo tiene la mayor altura.
# Muestre el resultado.

x = [
    [177,145,167,190,140,150,180,130], # grupo 1. sublista
    [165,176,145,210,170,189,159,190], # grupo 2  sublista
    [145,136,178,200,123,145,145,134], # grupo 3  sublista
    [201,110,187,175,156,165,156,135]  # grupo 4  sublista
]

# fomentar el uso de la siguiente forma para hacer iteraciones
# list comprehension

_, el_grupo_del_alto = max(
    [
        [max(grupo), n_grupo]
        for n_grupo, grupo in enumerate(x)
    ])

print(f'El grupo {el_grupo_del_alto + 1} tiene la mayor estatura.')

