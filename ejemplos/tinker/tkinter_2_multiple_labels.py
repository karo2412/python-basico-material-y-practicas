from tkinter import *

window = Tk()

window.title("Welcome to LikeGeeks app")

def clicked():

    lbl.configure(text="Button was clicked !!")


lbl = [Label(window, text=f"text {i}") for i in range(10)]

[i.grid(column=k, row=0) for k, i in enumerate(lbl)]

# lbl = Label(window, text="Hello", font=("Arial Bold", 50))

#lbl.grid(column=0, row=0)

#btn = Button(window, text="Click Me")
btn = Button(window, text="Click Me", command = clicked)
# btn = Button(window, text="Click Me", bg="orange", fg="red")

btn.grid(column=1, row=0)

window.geometry('350x200')
window.mainloop()