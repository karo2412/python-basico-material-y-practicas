
# Introduccion a las funciones en Python


```python
# Esto es un ejemplo de como escribir un hola mundo
print('hola mundo')
```

    hola mundo
    

## Práctica de funciones

### Ejemplo de Pitagoras


```python
# Importando la función matematica de la raíz cuadrada
from math import sqrt
```


```python
def hipotenusa(cateto1, cateto2):
    return cateto1**2 + cateto2**2
```


```python
hipotenusa(3,4)
```




    25




```python
a = 'lunes'; b='martes';c='sabado'
```


```python
f'hola: {a} {a} {a}'
```




    'hola: lunes lunes lunes'




```python
a = 3; b=4
```


```python
#tupla
```


```python
z = (a,b)
```


```python
z
```




    (3, 4)




```python
hipotenusa(3,4)
```




    25




```python
hipotenusa(*z)
```




    25




```python
z = [3,4]
```


```python
# unpack ...packing
```


```python
z = ('lunes', 'martes', 'miercoles', 'sabado')
```


```python
*c, a, _ = z
```


```python
a
```




    'miercoles'




```python
b
```




    'martes'




```python
c
```




    ['miercoles', 'sabado']




```python
a,b,c,d= z
```


```python
a
```




    'lunes'




```python
b
```




    'martes'




```python
c
```




    'miercoles'




```python
d
```




    'sabado'


