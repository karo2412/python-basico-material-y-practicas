[índice](README.md)

# NamedTuple

Otro tipo de tupla es la namedtuple que tiene un funcionamiento similar a la tupla simple pero adicionalmente sus componentes pueder ser accesados como si fueran atributos.

Primero se importa la biblioteca


```python
from collections import namedtuple
```

Por ejemplo se define una namedtuple que se llama 'Punto' y su colección de atributos.


```python
Point = namedtuple('Punto', ['x', 'y', 'z'])
```

Una namedtuple puede ser creada por medio de una llamada a la recien creada instancia 'Point' junto con sus parámetros.


```python
Point(1,2,3)
```




    Punto(x=1, y=2, z=3)



De igualmanera se puede asignar a una variable


```python
p = Point(1,2,3)
```


```python
p
```




    Punto(x=1, y=2, z=3)



Para Finalmente consultar por sus componentes


```python
p.x
```




    1




```python
p.y
```




    2




```python
p = Point(11, y=22, z=1) 
```


```python
p
```




    Punto(x=11, y=22, z=1)



Se puede crear la namedtuple mediante el uso de cualquier colección como elementos de entrada.


```python
t = [11,22,33]
Point._make(t)
```




    Punto(x=11, y=22, z=33)



De la misma manera se puede crear un diccionario apartir de dicha tupla.


```python
p._asdict()
```




    OrderedDict([('x', 11), ('y', 22), ('z', 1)])


[índice](README.md)