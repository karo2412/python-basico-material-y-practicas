# UNA ICAI Python Básico

Este es un repositorio para la documentación del curso

## Contenido

### Conceptos iniciales
1. [Introducción](Introducción.md)
1. [Números y aritmétrica](Números_y_Aritmetrica.md)
1. [Aritmétrica](Aritmétrica.md)
1. [Los textos en Python](Las_Cadenas_de_caracteres_o_Strings.md)
1. [Operaciones de los strings](Operaciones_en_Strings.md)
1. [Formateo de texto](Formateo_de_Strings.md)
1. [Condicionales](Conditionals.md)

### Estructuras de datos
1. [listas](Listas.md)
1. [tuplas](Tuplas.md)
1. [NamedTuples](NamedTuple.md)
1. [Conjuntos y Diccionarios](Conjuntos-y-diccionarios.md)
1. [Búsqueda en listas](Busqueda-de-elementos-comunes-en-listas.md)
1. [Como verificar si todos los elementos de una colección están en lista](Como-verficar-si-una-lista-es-una-subsecuencia-dentro-de-otra-lista.md)

### Control de flujo
1. [Control de Flujo](Control-de-flujo.md)
1. [Creación de un bloque de desición](Creación-de-un-bloque-de-decisiones.md)
1. [For loop](For-loops.md)

### Funciones
1. [Funciones](Funciones.md)
1. [Más sobre funciones](Ejemplos-de-asignación-de-parámetros-y-desempaque.md)