[índice](README.md)
# Como verficar si una lista es una subsecuencia dentro de otra lista
In [1]: `v1 = ['s', 'h', 'e', 'e', 'p']`
        `v2 = ['s', 's', 'h']`

`In [2]: v1 = sorted(v1)`

`In [3]: v2 = sorted(v2)`

`In [4]: def is_subseq(v2, v1):`

       """Comprueba si v2 es una subsecuencia de v1."""

       it = iter(v1)

       return all(c in it for c in v2)`

`In [5]: is_subseq(v2, v1)`
`Out[5]: False`

[índice](README.md)