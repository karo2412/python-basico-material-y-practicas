[índice](README.md)
## Como verificar si todos los elementos de una colección están en una lista
### Aproximación mediante sets

```python
l = ['a', 'b', 'c']
set(['a', 'b']) <= set(l)
```
Resultado

`True`

`False`

### Mediante método all() y comprensión

```python
l = ['a', 'b', 'c']
all(x in l for x in ['a', 'b'])
```
Resultado

`True`

```python
all(x in l for x in ['a', 'x'])
```

Resultado

`False`

### Mediante comprobación de subconjunto

```python
set(['a', 'b']).issubset(['a', 'b', 'c'])
```

Resultado

`True`

### Mediante comprobación de superconjunto

```python
l = ['a', 'b', 'c']
set(l).issuperset(set(['a','b']))
```

Resultado

`True`

### Mediante método all() y comprensión

```python
all(x in {'a', 'b', 'c'} for x in ['a', 'b'])
```

Resultado

`True`

### Mediante conjuntos por medio de {}

```python
{'a', 'b'}.issubset({'a', 'b', 'c'})
```
Resultado

`True`

[índice](README.md)