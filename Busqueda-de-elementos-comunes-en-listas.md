[índice](README.md)
# Busqueda de elementos comunes en listas

## Utilizando una lista de elementos


```python
frutas = ['apple', 'orange', 'lemon', 'cherry']
frutas_en_el_mercado = ['apple', 'orange', 'lemon', 'melon', 'grapes']
```


```python
def returnNotMatches(a, b):
    a = set(a)
    b = set(b)
    return [list(b - a), list(a - b)]
```

El primer elemento contiene las frutas que no tengo, el segundo elementos contiene los elementos no disponibles en el mercado


```python
returnNotMatches(frutas, frutas_en_el_mercado)
```




    [['grapes', 'melon'], ['cherry']]




```python
def intersection(a, b):
    a = set(a)
    b = set(b)
    return list(b & a)
```

Cuales elementos hay tanto como en mi caso y en el mercado


```python
intersection(frutas_en_el_mercado, frutas)
```




    ['apple', 'orange', 'lemon']




```python
def remove_on_list_from_sub_list(a, b):
    return [x for x in a if x not in b]
```

Fui al mercado con mi lista de frutas, compré casi todas excepto una.


```python
remove_on_list_from_sub_list(frutas,frutas_en_el_mercado)
```




    ['cherry']


[índice](README.md)