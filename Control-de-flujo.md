# Introducción
Barrido en una secuencia

    x = [ 1, 2, 3, 4, 5 ]
    for i in x:
        print('i is {}'.format(i))

Condicional mediante if

    if True:
        print('if true')
    elif False:
        print('elif true')
    else:
        print('neither true')

Condicional inline:

    hungry = True
    x = 'Feed the bear now!' if hungry else 'Do not feed the bear.'
    print(x)


Comparación de números:

	x = 42
	y = 73

	if x < y:
		print('x < y: x is {} and y is {}'.format(x, y))

## Ciclos mediante while
### Ejemplo fibonacci
	a, b = 0, 1
	while b < 1000:
		print(b, end = ' ', flush = True)
		a, b = b, a + b

	print() # line ending

## Ciclo indefinido mediante while

	secret = 'swordfish'
	pw = ''

	while pw != secret:
		pw = input("What's the secret word? ")


## Ciclos mediante for
	words = ['one', 'two', 'three', 'four', 'five']

	for i in words:
		print(i)

[For loops más detalles](https://github.com/PythonClassRoom/PythonClassBook/wiki/For-loops)

### Ciclo for contra una tupla

	animals = ( 'bear', 'bunny', 'dog', 'cat', 'velociraptor' )

	for pet in animals:
		print(pet)

