
# Creación de un bloque de decisiones

Se desea crear una estructura que permita invocar cualquier función o método mediante el uso de una llave o 'key'

Para ello se crean varios métodos:


```python
def caminar():
    print('estoy caminando')
```


```python
def correr():
    print('estoy corriendo')
```


```python
def mirar():
    print('estoy mirando')
```

La estructura en cuestión es un diccionario compuesto de varias llaves de manera mixta como palabrar o como cualquier otra coleción incluyendo enteros, tuplas.


```python
s = {
    'caminar' : caminar,
    'correr' : correr,
    'mirar' : mirar,
    'hablar' : print,
    'mover' : lambda x : x + 1,
    'int' : int,
    (1,1) : str,
    (1,2) : float
    
}
```

Dicha estructura dispone de referencias hacia un conjunto de métodos por invocar


```python
s
```




    {'caminar': <function __main__.caminar()>,
     'correr': <function __main__.correr()>,
     'mirar': <function __main__.mirar()>,
     'hablar': <function print>,
     'mover': <function __main__.<lambda>(x)>,
     'int': int,
     (1, 1): str,
     (1, 2): float}



A continuación unos ejemplos que permite llamar a los métodos que se tiene referencia:


```python
s['correr']()
```

    estoy corriendo
    


```python
s['caminar']()
```

    estoy caminando
    


```python
s['hablar']('hablo!!!')
```

    hablo!!!
    


```python
s['mover'](10)
```




    11




```python
s['int']('101')
```




    101




```python
s[(1,1)](10101)
```




    '10101'




```python
s[(1,2)]('3.14')
```




    3.14


